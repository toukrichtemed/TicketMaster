package com.ticketmaster.userservice.restcontrollers;

import com.ticketmaster.userservice.entities.Address;
import com.ticketmaster.userservice.entities.User;
import com.ticketmaster.userservice.exceptions.EmailAlreadyExistException;
import com.ticketmaster.userservice.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private ModelMapper modelMapper;

    @Test
    void shouldReturnListOfUsersByKeyword() throws Exception {
        //Given
        User user = new User();
        user.setEmail("test@test.com");
        user.setFirstName("John");
        user.setLastName("Doe");

        User user2 = new User();
        user2.setEmail("test2@test.com");
        user2.setFirstName("John2");
        user2.setLastName("Doe2");

        userService.createUser(user);
        userService.createUser(user2);
        List<User> usersList = new ArrayList<>();
        usersList.add(user);
        usersList.add(user2);
        Page<User> usersPage = new PageImpl<>(usersList, PageRequest.of(0,2), 2);

        //When
        when(userService.getUsers(anyString(), anyInt(), anyInt())).thenReturn(usersPage);

        RequestBuilder request = get("/users")
                .param("keyword","test")
                .param("offset","0")
                .param("limit","2");
        MvcResult result = mockMvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        //Then
        verify(userService).getUsers("test", 0,2);
    }

    @Test
    void shouldReturnInvalidInputListOfUsersByKeyword_400() throws Exception {

        RequestBuilder request = get("/users")
                .param("keyword","test")
                .param("offset","A")
                .param("limit","2");
        MvcResult result = mockMvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("invalid input, object invalid"))
                .andDo(print())
                .andReturn();

        verify(userService, never()).getUsers(anyString(), anyInt(),anyInt());
    }

    @Test
    void shouldCreateUser_200() throws Exception {
        //Given
        User user = new User();
        user.setEmail("test@test.com");
        user.setFirstName("John");
        user.setLastName("Doe");

        Address address = new Address();
        address.setAddress("4, Privet Drive2");
        address.setCity("Little Whinging");
        address.setCountry("Surrey, England");
        address.setZipcode("G1B2AA");

        user.setAddress(address);

        when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(post("/users")
                .accept(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"email\": \"test1@test2.com\",\n" +
                        "  \"firstName\": \"Johan\",\n" +
                        "  \"lastName\": \"Doe2\",\n" +
                        "  \"address\": {\n" +
                        "    \"address\": \"4, Privet Drive2\",\n" +
                        "    \"city\": \"Little Whinging\",\n" +
                        "    \"country\": \"Surrey, England\",\n" +
                        "    \"zipcode\": \"G1B2AA\"\n" +
                        "  }\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

    @Test
    void shouldNotCreateUserWhenEmailAlreadyExists_409() throws Exception {
        //Given
        when(userService.createUser(any())).thenThrow(EmailAlreadyExistException.class);

        mockMvc.perform(post("/users")
                .accept(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"email\": \"test1@test2.com\",\n" +
                        "  \"firstName\": \"Johan\",\n" +
                        "  \"lastName\": \"Doe2\",\n" +
                        "  \"address\": {\n" +
                        "    \"address\": \"4, Privet Drive2\",\n" +
                        "    \"city\": \"Little Whinging\",\n" +
                        "    \"country\": \"Surrey, England\",\n" +
                        "    \"zipcode\": \"G1B2AA\"\n" +
                        "  }\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(result -> assertEquals("email already exists", result.getResponse().getContentAsString()))
                .andDo(print())
                .andReturn();
    }

    @Test
    void shouldGetUserById() throws Exception {
        //Given
        User user = new User();
        user.setEmail("test@test.com");
        user.setFirstName("John");
        user.setLastName("Doe");

        userService.createUser(user);

        when(userService.getUserById(anyString())).thenReturn(user);

        mockMvc.perform(get("/users/4028b8817dce496b017dce49cd530000"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void shouldNotReturnGetUserById_404() throws Exception {

        when(userService.getUserById(anyString())).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(get("/users/bcd530000aabb"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertEquals("The request user is not found", result.getResponse().getContentAsString()));
    }
}
