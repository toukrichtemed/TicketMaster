package com.ticketmaster.userservice.repositories;

import com.ticketmaster.userservice.dtomappers.UserDTO;
import com.ticketmaster.userservice.entities.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @AfterEach
    void tearDown(){
        userRepository.deleteAll();
    }

    @Test
    void shouldFindAllUsersByKeyword() {
        //Given
        User user = new User();
        user.setEmail("test@test.com");
        user.setFirstName("John");
        user.setLastName("Doe");

        User user2 = new User();
        user2.setEmail("test2@test.com");
        user2.setFirstName("John2");
        user2.setLastName("Doe2");

        userRepository.save(user);
        userRepository.save(user2);
        List<User> usersList = new ArrayList<>();
        usersList.add(user);
        usersList.add(user2);
        Page<User> usersPage = new PageImpl<>(usersList, PageRequest.of(0,2), 2);
        //When
        Page<User> responseSearchInemail = userRepository.findAllByKeyword("test", PageRequest.of(0, 2));
        Page<User> responseSearchInFirstName = userRepository.findAllByKeyword("John", PageRequest.of(0, 2));
        Page<User> responseSearchInLastName = userRepository.findAllByKeyword("Doe", PageRequest.of(0, 2));
        //Then
        assertThat(responseSearchInemail).isEqualTo(usersPage);
        assertThat(responseSearchInFirstName).isEqualTo(usersPage);
        assertThat(responseSearchInLastName).isEqualTo(usersPage);
    }

    @Test
    void shouldReturnEmptyPageWhenKeywordIsNotFound() {
        //Given
        User user = new User();
        user.setEmail("test@test.com");
        user.setFirstName("John");
        user.setLastName("Doe");

        User user2 = new User();
        user2.setEmail("test2@test.com");
        user2.setFirstName("John2");
        user2.setLastName("Doe2");

        userRepository.save(user);
        userRepository.save(user2);
        List<User> usersList = new ArrayList<>();
        usersList.add(user);
        usersList.add(user2);
        Page<User> usersPage = new PageImpl<>(usersList, PageRequest.of(0,2), 2);
        //When
        Page<User> responseSearchInemail = userRepository.findAllByKeyword("blablaEmail", PageRequest.of(0, 2));
        Page<User> responseSearchInFirstName = userRepository.findAllByKeyword("blablaFirstName", PageRequest.of(0, 2));
        Page<User> responseSearchInLastName = userRepository.findAllByKeyword("blablaLastName", PageRequest.of(0, 2));
        //Then
        assertThat(responseSearchInemail).isNotEqualTo(usersPage);
        assertThat(responseSearchInFirstName).isNotEqualTo(usersPage);
        assertThat(responseSearchInLastName).isNotEqualTo(usersPage);
    }

    @Test
    void shouldReturnTrueIfUserEmailexists() {
        //Given
        User user = new User();
        String email = "test@test.com";
        user.setEmail(email);
        user.setFirstName("John");
        user.setLastName("Doe");
        userRepository.save(user);
        //When
        boolean exists = userRepository.existsByEmail(email);
        //Then
        assertThat(exists).isTrue();

    }
    @Test
    void shouldReturnFalseIfUserEmailDoesNotexist() {
        //Given
        String email = "test@test.com";
        //When
        boolean exists = userRepository.existsByEmail(email);
        //Then
        assertThat(exists).isFalse();

    }
}
