package com.ticketmaster.userservice.services;

import com.ticketmaster.userservice.entities.User;
import com.ticketmaster.userservice.exceptions.EmailAlreadyExistException;
import com.ticketmaster.userservice.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import javax.persistence.EntityNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    private UserService userService;

    @BeforeEach
    void setUp(){
        userService = new UserServiceImpl(userRepository);
    }

    @Test
    void shouldGetUsersByKeyword() {
        userService.getUsers("test", 0,2);
        verify(userRepository).findAllByKeyword("test", PageRequest.of(0,2));
    }

    @Test
    void shouldCreateUser() {
        //Given
        User user = new User();
        String email = "test@test.com";
        user.setEmail(email);
        user.setFirstName("John");
        user.setLastName("Doe");
        userService.createUser(user);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());

        User capturedUser = userArgumentCaptor.getValue();
        assertThat(capturedUser).isEqualTo(user);
    }

    @Test
    void shouldThrowInCreateUserWhenEmailAlreadyExists() {
        //Given
        User user = new User();
        String userId = "4028b8817dce3cad017dce3cc8860000";
        String email = "test@test.com";
        user.setId(userId);
        user.setEmail(email);
        user.setFirstName("John");
        user.setLastName("Doe");

        given(userRepository.existsByEmail(anyString()))
                .willReturn(true);

        //When
        assertThatThrownBy(() -> userService.createUser(user))
                .isInstanceOf(EmailAlreadyExistException.class);
        //Then
        verify(userRepository, never()).save(any());
    }

    @Test
    void shouldGetUserById() {
        //Given
        User user = new User();
        String userId = "4028b8817dce3cad017dce3cc8860000";
        String email = "test@test.com";
        user.setId(userId);
        user.setEmail(email);
        user.setFirstName("John");
        user.setLastName("Doe");
        userService.createUser(user);
        //When
        when(userRepository.getById(userId)).thenReturn(user);
        User returnedUser = userRepository.getById(userId);
        //Then
        assertNotNull(returnedUser);
        assertEquals(returnedUser.getId(), userId);
        assertEquals(returnedUser.getEmail(), email);
    }

    @Test
    void shouldThrowWhenUserByIdNotFound() {
        when(userRepository.getById(anyString())).thenThrow(EntityNotFoundException.class);
        assertThatThrownBy(() -> userService.getUserById(anyString()))
                .isInstanceOf(EntityNotFoundException.class);
    }
}
