package com.ticketmaster.userservice.errorhandlers;

import com.ticketmaster.userservice.exceptions.EmailAlreadyExistException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ErrorHandlerController{

    private static final String FAILED_VALIDATION_FOR_INPUT_FIELDS = "invalid input, object invalid";
    private static final String USER_NOT_FOUND = "The request user is not found";
    private static final String EMAIL_ALREADY_EXISTS = "email already exists";

    @ExceptionHandler({ConstraintViolationException.class, MethodArgumentNotValidException.class})
    public ResponseEntity<ResponseError> handleMethodArgumentNotValidException(ConstraintViolationException exception){
        List<ErrorModel> errorModels = new ArrayList<>();
        errorModels = exception.getConstraintViolations()
                .stream()
                .map(fieldError -> new ErrorModel(fieldError.getPropertyPath().toString(), fieldError.getMessageTemplate()))
                .distinct()
                .collect(Collectors.toList());

        ResponseError errors = new ResponseError(FAILED_VALIDATION_FOR_INPUT_FIELDS, errorModels);
        return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity handleEntityNotFoundException(){
        return new ResponseEntity(USER_NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = EmailAlreadyExistException.class)
    public ResponseEntity handleEmailAlreadyExistsException(EmailAlreadyExistException exception) {
        return new ResponseEntity(EMAIL_ALREADY_EXISTS, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ResponseError> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
        List<ErrorModel> errorModels = new ArrayList<>();
        ErrorModel errorModel = new ErrorModel(exception.getParameter().getParameterName(), exception.getLocalizedMessage());
        List<ErrorModel>  errors = new ArrayList<ErrorModel>();
        errorModels.add(errorModel);
        ResponseError error = new ResponseError(FAILED_VALIDATION_FOR_INPUT_FIELDS, errorModels);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }
}
