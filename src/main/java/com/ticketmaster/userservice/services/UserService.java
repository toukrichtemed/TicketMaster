package com.ticketmaster.userservice.services;

import com.ticketmaster.userservice.entities.User;
import com.ticketmaster.userservice.exceptions.EmailAlreadyExistException;
import org.springframework.data.domain.Page;

public interface UserService {

    Page<User> getUsers(String keyword, Integer offset, Integer limit);

    User createUser(User user) throws EmailAlreadyExistException;

    User getUserById(String id);
}
