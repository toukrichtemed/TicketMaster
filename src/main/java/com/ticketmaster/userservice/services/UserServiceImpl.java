package com.ticketmaster.userservice.services;

import com.ticketmaster.userservice.entities.User;
import com.ticketmaster.userservice.exceptions.EmailAlreadyExistException;
import com.ticketmaster.userservice.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Page<User> getUsers(String keyword, Integer offset, Integer limit){
        logger.info("==> UserServiceImpl getUsers {}, {}, {}"+keyword, offset, limit);
        return userRepository.findAllByKeyword(keyword, PageRequest.of(offset, limit));
    }

    @Override
    public User createUser(User user){
        logger.info("==> UserServiceImpl CreateUser()");
        if(userRepository.existsByEmail(user.getEmail())){
            logger.error("==> UserServiceImpl User with email {} already exists", user.getEmail());
            throw new EmailAlreadyExistException();
        }
        return userRepository.save(user);
    }

    @Override
    public User getUserById(String id){
        logger.info("==> UserServiceImpl getUserById() for param: userId: {}", id);
        return userRepository.getById(id);
    }
}
