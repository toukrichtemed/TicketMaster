package com.ticketmaster.userservice.restcontrollers;

import com.ticketmaster.userservice.dtomappers.UserDTO;
import com.ticketmaster.userservice.entities.User;
import com.ticketmaster.userservice.services.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users")
@Validated
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * users return the list of users based on search using @param keyword
     * @param keyword
     * @param offset
     * @param limit
     * @return a page of users with offset and max result
     */
    @GetMapping
    public List<UserDTO> users(@RequestParam(value = "keyword", defaultValue = "", required = false) String keyword,
                               @RequestParam("offset") @Min(0) Integer offset,
                               @RequestParam("limit") @Min(0) @Max(50) Integer limit){
        logger.info("==> UserController users() for prams: keyword: {}, offset: {} and limit: {}", keyword, offset, limit);
        return userService
                .getUsers(keyword, offset, limit).stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * Creates a user after checking if the user email does not exist already
     * @param userDTO
     * @return
     */
    @PostMapping
    public ResponseEntity createUser(@Valid @RequestBody UserDTO userDTO){
        logger.info("==> UserController CreateUser()");
        User user = modelMapper.map(userDTO, User.class);
        UserDTO userCreated = modelMapper.map(userService.createUser(user), UserDTO.class);
        return new ResponseEntity(userCreated, HttpStatus.OK);
    }

    /**
     * Return a user by it ID
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity getUserById(@PathVariable(name = "id") String id){
        logger.info("==> UserController getUserById() for param: userId: {}", id);
        User user =  userService.getUserById(id);
        return ResponseEntity
                .ok()
                .body(modelMapper.map(user, UserDTO.class));

    }
}
