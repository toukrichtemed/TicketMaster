package com.ticketmaster.userservice.exceptions;

public class EmailAlreadyExistException extends RuntimeException {

    private String message;

    public EmailAlreadyExistException(String message) {
        this.message = message;
    }
    public EmailAlreadyExistException() {
    }
}
