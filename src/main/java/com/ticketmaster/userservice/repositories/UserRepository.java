package com.ticketmaster.userservice.repositories;

import com.ticketmaster.userservice.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

    /**
     * This query search by keyword on multiple fields in User table (i.e: firstName, lastName and email)
     * @param keyword
     * @param pageable
     * @return
     */
    @Query("FROM User u WHERE u.firstName like %:keyword% OR u.lastName like %:keyword% OR u.email like %:keyword%")
    Page<User> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);

    boolean existsByEmail(String email);

}
