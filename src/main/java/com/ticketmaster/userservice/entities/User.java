package com.ticketmaster.userservice.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "USER")
public class User{

    @Id
    @GeneratedValue(generator="uuid")
    @GenericGenerator(name="uuid", strategy = "uuid")
    @Column(name = "ID")
    private String id;

    @Column(name = "EMAIL", unique = true)
    @NotNull
    @NotBlank
    @Pattern(regexp = "[\\w\\d-]+@[\\w]+[.][a-z]+")
    private String email;

    @Column(name = "FIRST_NAME")
    @NotNull
    @NotBlank
    private String firstName;

    @Column(name = "LAST_NAME")
    @NotNull
    @NotBlank
    private String lastName;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
