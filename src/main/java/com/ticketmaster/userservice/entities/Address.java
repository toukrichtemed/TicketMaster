package com.ticketmaster.userservice.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "ADDRESS")
public class Address{

    @Id
    @GeneratedValue(generator="uuid")
    @GenericGenerator(name="uuid", strategy = "uuid")
    @Column(name = "ID")
    private String id;

    @Column(name = "ADDRESS")
    @NotNull
    @NotBlank
    private String address;

    @Column(name = "CITY")
    @NotNull
    @NotBlank
    private String city;

    @Column(name = "COUNTRY")
    @NotNull
    @NotBlank
    private String country;

    @Column(name = "ZIPCODE")
    @NotNull
    @NotBlank
    @Pattern(regexp = "[A-Z0-9]{6}")
    private String zipcode;

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
