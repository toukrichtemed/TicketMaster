# TicketMaster a Spring boot, MySQL, API, Spring data Rest API

This project aims to simulates the **User service** as a micro-service that manages users throw three main endpoints, and it is running on docker. 

## Requirements

1. `Java 1.8.x`

2. `Maven 3.x.x`

3. `Mysql 8.0.x`

4. `Docker 2.x.x.x`

## Steps to Setup

You have two ways to test this project, whether on your local or using dedicated containers on docker 

#### Test on your local

1. Clone ticket master project 
    ``` 
    git clone https://gitlab.com/toukrichtemed/TicketMaster.git
    ```
2. Install MySQL and create a database called "ticketmaster"

3. Change mysql configuration as per your installation

    - [ ] Open `src/main/resources/application.properties`
    - [ ] Change `spring.datasource.username` and `spring.datasource.password` as per your mysql installation.
    - [ ] Change `spring.datasource.url` and give the host {+localhost+} instead of {-mysql-standalone-}.
    - [ ] Delete `server.port` configuration in order to run the application on default port `8080`.


4. Build and run the app using maven
    ```
    mvn package
    java -jar target/userservice-1.0.0.jar
    ```
    Alternatively, you can run the app without packaging it using:
    ```
    mvn spring-boot:run
    ```
    The app will start running at [http://localhost:8080](http://localhost:8080).

#### Test using docker containers

1. Clone ticket master project 
    ``` 
    git clone https://gitlab.com/toukrichtemed/TicketMaster.git
    ```
2. Setup MySQL container and create a database called "ticketmaster"
    - [ ] Install docker and Pull a MySQL image from DockerHub using:

        ```
        docker pull mysql:latest
        ```
    - [ ] Following to this, you will run the MySQL server to run as a Docker container using:

        ```
        docker run -d -p 3306:3306 --name mysql-standalone -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=ticketmaster -d mysql:latest 
        ```
    :zap: The database `ticketmaster` is created automatically after running the command above.
    

3. Setup userservice container 
    - [ ] Pull the userservice image from dockerHub using:

            docker pull toukmed/userservice:latest

    OR

    - [ ] Build the spring boot service image from the directory where Dockerfile is placed

            docker build -f Dockerfile -t userservice:latest .

    - [ ] Create, link to mysql-standalone container and run the userservice container using:

            docker run -d -p 8089:8089 --name userservice-standalone --link mysql-standalone userservice:latest

    The app will start running at [http://localhost:8089](http://localhost:8089).

## Explore Rest APIs

#### GET /users    
      Return the list of users defined by a keyword contained in the first name, last name or email of the user.
    
        Exemple of Get users request:
            curl -X GET \
                'http://localhost:8089/users?keyword=test&offset=0&limit=10' \
                -H 'cache-control: no-cache' \
                -H 'limit: 10' \
                -H 'offset: 0' \
                -H 'postman-token: b6788464-0fcf-4785-7cc6-ffb467262048'

#### POST /users
      Create a user into database in case no user with the same email already exists.

        Exemple of Create user request:
            curl -X POST \
                http://localhost:8089/users \
                -H 'cache-control: no-cache' \
                -H 'content-type: application/json' \
                -H 'postman-token: 3ca8f6ca-fd3c-bb3f-e653-c4980dd263c6' \
                -d '{
                "email": "john@doe.com",
                "firstName": "John",
                "lastName": "Doe",
                "address": {
                    "address": "4, Privet Drive2",
                    "city": "Little Whinging",
                    "country": "Surrey, England",
                    "zipcode": "G1B2AA"
                }
                }'

#### GET /users/{id}
      Find a user in database with his id.

        Exemple of Get user by id request:
            curl -X GET \
                http://localhost:8089/users/4028b8817dceb0d3017dceb94d8d0006 \
                -H 'cache-control: no-cache' \
                -H 'postman-token: 3549b8c5-bca5-edd1-a286-b9aa927dbe03'
    

## CI/CD Pipline

In the file .gitlab-ci.yml you will find a description of the pipline put in place to run basically on two stages `build` and `docker`.

- [ ] The build stage is done using maven3 and jdk8, it packages the the application and install it.
- [ ] The docker stage login, build the image and push it to Gitlab registry, you can check it throw [this link](https://gitlab.com/toukrichtemed/TicketMaster/container_registry). 
