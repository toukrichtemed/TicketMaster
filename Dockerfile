FROM openjdk:8
ADD target/userservice-1.0.0.jar userservice-1.0.0.jar
EXPOSE 8089
ENTRYPOINT ["java", "-jar", "userservice-1.0.0.jar"]
